
<?php get_header();?>
        <div class="content">
            <div class="container">

				<!-- START: PAGE CONTENT -->
                <div class="blog">
                    <div class="blog-grid">
                        <div class="grid-sizer"></div>
                      

    <?php if (have_posts()):while (have_posts()): the_post();?>
          <?php get_template_part('content');?>
    <?php endwhile; endif; ?>
     </div>
 <div class="pagination">
     <?php the_posts_pagination( array(
    'mid_size' => 2,
    'prev_text' => __( 'Previous', 'textdomain' ),
    'next_text' => __( 'Next', 'textdomain' ),
    ) ); ?>
 </div>
    </div>
    </div>
    </div>
    </div>
<?php get_footer();?>

