<!-- START: PAGE CONTENT -->

<div class="grid-item">
                            <article class="post-box animate-up">
                                <div class="post-media">
                                    <div class="post-image">
                                        <a href="single.html">                                 
                                        <?php
        
                                            if ( has_post_thumbnail() )
                                             the_post_thumbnail();
    
                                             // if ( has_post_thumbnail() ) the_post_thumbnail( 'thumbnail' );//
    
                                        ?>
                                       </a>
                                    </div>
                                </div>

                                <div class="post-data">
                                    <time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">

                                    
                                    <span class="day">
                                    <?php echo get_the_date("d"); ?></span>
                                        <span class="month">MAY</span>
                                    </time>

                                    <div class="post-tag">
                                        <a href="category.html">#Photo</a>
                                        <a href="category.html">#Architect</a>
                                    </div>

                                    <h3 class="post-title">
                                    <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                    </h3>

                                    <div class="post-info">
                                        <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-user"><?php the_author(); ?></i></a>
                                        <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number();?></a>
                                    </div>
                                </div>
                            </article>
                        </div>


