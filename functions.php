<?php 
// addscriptsand and stylessheets

add_action('wp_enqueue_scripts', 'custom_scripts');
function custom_scripts()
{

	
    wp_enqueue_style('open-sans', "https://fonts.googleapis.com/css?family=Fredoka+On");


    wp_enqueue_style('open-sans', "https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic");

    wp_enqueue_style('map-icons', get_bloginfo('template_directory') . '/fonts/map-icons/css/map-icons.min.css' );
    wp_enqueue_style('icon-fonts',get_bloginfo('template_directory') . '/fonts/icomoon/style.css');
    wp_enqueue_style('bxslider',get_bloginfo('template_directory') . '/js/plugins/jquery.bxslider/jquery.bxslider.css');   
    wp_enqueue_style('mCustomScrollbar',get_bloginfo('template_directory') . '/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.min.css');
    wp_enqueue_style('mediaelementplayer',get_bloginfo('template_directory') . '/js/plugins/jquery.mediaelement/mediaelementplayer.min.css');
    wp_enqueue_style('fancybox',get_bloginfo('template_directory') . '/js/plugins/jquery.fancybox/jquery.fancybox.css');   
    wp_enqueue_style('owlcarousel',get_bloginfo('template_directory') . '/js/plugins/jquery.owlcarousel/owl.carousel.css');   
    wp_enqueue_style('theme-carousel',get_bloginfo('template_directory') . '/js/plugins/jquery.owlcarousel/owl.theme.css');   
    wp_enqueue_style('option-panel',get_bloginfo('template_directory') . '/js/plugins/jquery.optionpanel/option-panel.css');   
    wp_enqueue_style('style-sheet',get_bloginfo('template_directory') . '/style.css');  
    wp_enqueue_style('color',get_bloginfo('template_directory') . '/colors/theme-color.css');


    wp_enqueue_script('ajax-googleapis',  'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js', array(),'3.3.6',true);
    wp_enqueue_script('maps-googleapis', 'https://maps.googleapis.com/maps/api/js', array(),'3.3.6',true);
    wp_enqueue_script('js-site', get_template_directory_uri() . '/js/site.js', array(),'3.3.6',true);

}


add_action('after_setup_theme', 'custom_theme_setup');
function custom_theme_setup()
{
    // wordpress Titles
    add_theme_support('title-tag');

    add_theme_support('post-thumbnails');

    register_nav_menus(array(
        'primary'=> 'Menu principale',

    ));
}


if ( function_exists('acf_add_options_page')){
    acf_add_options_page();

}