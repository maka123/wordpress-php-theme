<div class="sidebar sidebar-fixed">
        <button class="btn-sidebar btn-sidebar-close"> <i class="rsicon rsicon-close"></i></button>

        <div class="widget-area">
            <aside class="widget widget-profile animate-up">
                <div class="profile-photo">
                    <img src="img/uploads/rs-photo-v2.jpg" alt="Robert Smith"/>
                </div>
                <div class="profile-info">
                    <h2 class="profile-title"><?php the_author_meta('first_name');?></h2>
                    <h3 class="profile-position"><?php the_author_meta('description');?></h3>
                </div>
            </aside><!-- .widget-profile -->

            <aside class="widget widget_search animate-up">
                <h2 class="widget-title">Search</h2>
                <form class="search-form">
                    <label class="ripple">
                        <span class="screen-reader-text">Search for:</span>
                        <input class="search-field" type="search" placeholder="Search">
                    </label>
                    <input type="submit" class="search-submit" value="Search">
                </form>
            </aside><!-- .widget_search -->

            <aside class="widget widget_contact animate-up">
                <h2 class="widget-title">Contact Me</h2>
                <form class="contactForm" action="https://rscard.px-lab.com/html/php/contact_form.php" method="post">
					<div class="input-field">
						<input class="contact-name" type="text" name="name"/>
						<span class="line"></span>
						<label>Name</label>
					</div>

					<div class="input-field">
						<input class="contact-email" type="email" name="email"/>
						<span class="line"></span>
						<label>Email</label>
					</div>

					<div class="input-field">
						<input class="contact-subject" type="text" name="subject"/>
						<span class="line"></span>
						<label>Subject</label>
					</div>

					<div class="input-field">
						<textarea class="contact-message" rows="4" name="message"></textarea>
						<span class="line"></span>
						<label>Message</label>
					</div>

					<span class="btn-outer btn-primary-outer ripple">
						<input class="contact-submit btn btn-lg btn-primary" type="submit" value="Send"/>
					</span>
					
					<div class="contact-response"></div>
				</form>
            </aside><!-- .widget_contact -->

            <aside class="widget widget-popuplar-posts animate-up">
                <h2 class="widget-title">Popular posts</h2>
                <ul>
                    <li>
                        <div class="post-media"><a href="category.html"><img src=" <?php
        
        if ( has_post_thumbnail() )
         the_post_thumbnail();

         // if ( has_post_thumbnail() ) the_post_thumbnail( 'thumbnail' );//

    ?></a></div>
                        <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number();?></a></div>
                    </li>
                    <li>
                    <div class="post-media"><a href="category.html"><img src="<img src=" <?php
        
        if ( has_post_thumbnail() )
         the_post_thumbnail();

         // if ( has_post_thumbnail() ) the_post_thumbnail( 'thumbnail' );//

    ?></a></div>
                        <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number();?></a></div>
                    </li>
                    <li>
                        <div class="post-media"><a href="category.html"><img src="<img src=" <?php
        
        if ( has_post_thumbnail() )
         the_post_thumbnail();

         // if ( has_post_thumbnail() ) the_post_thumbnail( 'thumbnail' );//

    ?></a></div>
                        <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number();?></a></div>
                    </li>
                </ul>
            </aside><!-- .widget-popuplar-posts -->

            <aside class="widget widget_tag_cloud animate-up">
                <h2 class="widget-title">Tag Cloud</h2>
                <div class="tagcloud">
                    <a href="category.html" title="1 topic">Business</a>
                    <a href="category.html" title="9 topics">City</a>
                    <a href="category.html" title="10 topics">Creative</a>
                    <a href="category.html" title="6 topics">Fashion</a>
                    <a href="category.html" title="2 topics">Music</a>
                    <a href="category.html" title="5 topics">News</a>
                    <a href="category.html" title="9 topics">Peoples</a>
                </div>
            </aside><!-- .widget_tag_cloud -->

            <aside class="widget widget-recent-posts animate-up">
                <h2 class="widget-title">Recent posts</h2>
                <ul>
                    <li>
                        <div class="post-tag">
                            <a href="category.html">#Photo</a>
                            <a href="category.html">#Architect</a>
                        </div>
                                         <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number();?></a></div>
                    </li>
                    <li>
                        <div class="post-tag">
                            <a href="category.html">#Photo</a>
                            <a href="category.html">#Architect</a>
                        </div>
                        <h3 class="post-title"><a href="category.html">Standard Post Format With Featured Image</a></h3>
                        <div class="post-info"><a href="category.html"><i class="rsicon rsicon-comments"></i>56 comments</a></div>
                    </li>
                    <li>
                        <div class="post-tag">
                            <a href="category.html">#Photo</a>
                            <a href="category.html">#Architect</a>
                        </div>
                        <h3 class="post-title"><a href="category.html">Standard Post Format With Featured Image</a></h3>
                        <div class="post-info"><a href="category.html"><i class="rsicon rsicon-comments"></i>56 comments</a></div>
                    </li>
                </ul>
            </aside><!-- .widget-recent-posts -->

            <aside class="widget widget_categories animate-up">
                <h2 class="widget-title">Categories</h2>
                <ul>
                    <li><a href="category.html" title="Architecture Category Posts">Architecture</a> (9)</li>
                    <li><a href="category.html" title="Business Category Posts">Business</a> (16)</li>
                    <li><a href="category.html" title="Creative Category Posts">Creative</a> (18)</li>
                    <li><a href="category.html" title="Design Category Posts">Design</a> (10)</li>
                    <li><a href="category.html" title="Development Category Posts">Development</a> (14)</li>
                    <li><a href="category.html" title="Education Category Posts">Education</a> (9)</li>
                </ul>
            </aside><!-- .widget_categories -->
        </div><!-- .widget-area -->
    </div><!-- .sidebar -->